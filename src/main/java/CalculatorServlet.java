import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@WebServlet("/calculator")
public class CalculatorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String firstNumber = request.getParameter("firstNumber");
        String secondNumber = request.getParameter("secondNumber");
        String operation = request.getParameter("operation");
        double firstNumberValue = 0;
        double secondNumberValue = 0;
        boolean noError = true;

        try {
            firstNumberValue = Double.parseDouble(firstNumber);
            secondNumberValue = Double.parseDouble(secondNumber);
        } catch (Exception ex) {
            noError = false;
        }

        if (noError) {
            double result = 0;
            try {
                if (operation.equals("+")) result = functionSum(firstNumberValue, secondNumberValue);
                else if (operation.equals("-")) result = functionDif(firstNumberValue, secondNumberValue);
                else if (operation.equals("*")) result = functionMul(firstNumberValue, secondNumberValue);
                else if (operation.equals("/") && (secondNumberValue != 0)) result = functionDiv(firstNumberValue, secondNumberValue);
                else
                    noError = false;
            } catch (Exception ex) {
                noError = false;
            }

            if (noError) {
                doSetResult(response, result);
                return;
            }
        }
        doSetError(response);
    }

    protected void doSetResult(HttpServletResponse response, double result) throws UnsupportedEncodingException, IOException {
        String reply = "{\"error\":0,\"result\":" + Double.toString(result) + "}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    protected void doSetError(HttpServletResponse response) throws UnsupportedEncodingException, IOException {
        String reply = "{\"error\":1}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    protected double functionSum(double a, double b) {
        return a + b;
    }

    protected double functionDif(double a, double b) {
        return a - b;
    }

    protected double functionMul(double a, double b) {
        return a * b;
    }

    protected double functionDiv(double a, double b) {
        return a / b;
    }
}
